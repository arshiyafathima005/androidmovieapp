package com.example.linearlogin.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.model.RecyclerDynamicModel;
import java.util.List;

public class RecylerDynamicAdapter extends RecyclerView.Adapter<RecylerDynamicAdapter.RecyclerHolder> {
    private List<RecyclerDynamicModel> dynamicModelList;

    public RecylerDynamicAdapter(List<RecyclerDynamicModel> dynamicModelList)
    {
        this.dynamicModelList = dynamicModelList;
    }

    @NonNull
    @Override
    public RecyclerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.dynamic_recycler,parent,false);
        return new RecyclerHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull RecyclerHolder holder, int position) {
        RecyclerDynamicModel model = dynamicModelList.get(position);
        holder.recyclerText.setText(model.getRecyclerText());
        holder.headText.setText(model.getHeadText());
        Glide.with(holder.recyclerImage.getContext())
                .load(model.getRecyclerImage())
                .into(holder.recyclerImage);
    }
    public void setData(List<RecyclerDynamicModel> multipleView)
    {
        if(multipleView!=null && multipleView.size()>0)
        {
            this.dynamicModelList=multipleView;
            notifyDataSetChanged();
        }
    }
    @Override
    public int getItemCount()
    {
        return dynamicModelList.size();
    }
    public class RecyclerHolder extends RecyclerView.ViewHolder {
        private TextView recyclerText,headText;
        private ImageView recyclerImage;

        public RecyclerHolder(@NonNull View itemView) {
            super(itemView);
            recyclerText = (TextView)itemView.findViewById(R.id.recyclerIteller);
            headText = (TextView)itemView.findViewById(R.id.recyclerText);
            recyclerImage = itemView.findViewById(R.id.recyclerImage);
        }
    }
}
