package com.example.linearlogin.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.movies.api.MovieModel;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {
    private JsonArray reviewModels;
    public ReviewAdapter(JsonArray reviewModels) {
        this.reviewModels=reviewModels;

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.activity_review_list_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
      /* ReviewModel model=reviewModels.get(position);
       holder.review.setText(model.getReview());
       holder.date.setText(model.getDate());
        Glide.with(holder.reviewImage.getContext())
                .load(model.getReviewImage())
                .into(holder.reviewImage);*/
        JsonObject model=reviewModels.get(position).getAsJsonObject();
       try {
           if (model.has("name"))//
           {
               holder.review.setText(model.get("name").getAsString());
           }
           if(model.has("createdby"))
           {
               holder.date.setText(model.get("createdby").getAsString());
           }
           if(model.has("imageurl"))
           {
               Glide.with(holder.reviewImage.getContext())
                       .load(model.get("imageurl").getAsString())
                       .into(holder.reviewImage);
           }
       }catch (Exception e)
       {
           e.printStackTrace();
       }


        //holder.review.setText(model.get("name").getAsString());
      /*  holder.date.setText(model.get("createdby").getAsString());
        Glide.with(holder.reviewImage.getContext())
                .load(model.get("imageurl").getAsString())
                .into(holder.reviewImage);*/
          Log.d("ReviewAdapter", "onBindViewHolder: "+model);

    }
    @Override
    public int getItemCount() {
        return reviewModels.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder
    {
 private TextView review,date;
 private ImageView reviewImage;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            review=itemView.findViewById(R.id.uName);
            date=itemView.findViewById(R.id.uEmail);
            reviewImage=itemView.findViewById(R.id.userProfile);
        }
    }

}
