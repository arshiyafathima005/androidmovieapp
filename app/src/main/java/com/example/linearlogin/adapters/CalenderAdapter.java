package com.example.linearlogin.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.model.ReviewHotelModel;

import java.util.List;

public class CalenderAdapter extends RecyclerView.Adapter<CalenderAdapter.CalenderHolder> {
    private List<ReviewHotelModel> calender;

    public CalenderAdapter(List<ReviewHotelModel> calender)
    {
        this.calender = calender;
    }

    @NonNull
    @Override
    public CalenderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.calender,parent,false);
        return new CalenderHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull CalenderHolder holder, int position) {
        ReviewHotelModel model = calender.get(position);
        holder.day.setText(model.getDay());
        holder.date.setText(model.getDate());
    }
    @Override
    public int getItemCount()
    {
        return calender.size();
    }
    public class CalenderHolder extends RecyclerView.ViewHolder{
        private TextView day,date;
        public CalenderHolder(@NonNull View itemView) {
            super(itemView);
            day=(TextView)itemView.findViewById(R.id.dayCal);
            date=(TextView)itemView.findViewById(R.id.dateCal);
        }
    }

    public class CardHolder {
    }
}
