package com.example.linearlogin.adapters;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.linearlogin.R;
import com.example.linearlogin.model.ModelViewPager;
import com.flaviofaria.kenburnsview.KenBurnsView;
import com.squareup.picasso.Picasso;

import java.util.List;
public class ViewPgersTwoAdapter extends RecyclerView.Adapter<ViewPgersTwoAdapter.ViewHolder> {
    private List<ModelViewPager> location;
    public ViewPgersTwoAdapter(List<ModelViewPager> location) {
        this.location = location;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.custom_viewpager,parent,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ModelViewPager item=location.get(position);
        Picasso.get().load(item.image).into(holder.image);
        holder.name.setText(item.name);
        holder.description.setText(item.description);
    }
    @Override
    public int getItemCount() {
        return location.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView name,description;
        private KenBurnsView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.placeName);
            description = itemView.findViewById(R.id.description);
            image = itemView.findViewById(R.id.kbImage);
        }
        }
}
