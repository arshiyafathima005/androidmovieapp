package com.example.linearlogin.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.model.BookModel;
import com.example.linearlogin.model.ReviewHotelModel;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookHolder>{
        private List<BookModel> book;

        public BookAdapter(List<BookModel> book) {
        this.book = book;
        }
        @NonNull
    @Override
    public BookHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.book_recycler,parent,false);
        return new BookHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull BookAdapter.BookHolder holder, int position) {
        BookModel model = book.get(position);
        holder.bookAuthor.setText(model.getBookAuthor());
        holder.bookTitle.setText(model.getBookTitle());
        Glide.with(holder.imageBook.getContext())
                .load(model.getBookImage())
                .into(holder.imageBook);
    }
    @Override
    public int getItemCount() {
        return book.size();
    }
    public class BookHolder extends RecyclerView.ViewHolder{
        private TextView bookAuthor,bookTitle;
        private ImageView imageBook;
        public BookHolder(@NonNull View itemView) {
            super(itemView);
            bookAuthor=(TextView)itemView.findViewById(R.id.bookAuthor);
            bookTitle=(TextView)itemView.findViewById(R.id.bookTitile);
            imageBook=itemView.findViewById(R.id.imageBook);
        }
    }
}

