package com.example.linearlogin.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class AdapterJsons extends RecyclerView.Adapter<AdapterJsons.JsonHolder>{
    private JsonArray jArray;
    public AdapterJsons(JsonArray jArray) {
        this.jArray = jArray;
    }

    @NonNull
    @Override
    public JsonHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.json_custom_recycler,parent,false);
        return new JsonHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JsonHolder holder, int position) {
        JsonObject model=jArray.get(position).getAsJsonObject();
        try {
            if (model.has("name"))//
            {
                holder.name.setText(model.get("name").getAsString());
            }
            if(model.has("createdby"))
            {
                holder.createdBy.setText(model.get("createdby").getAsString());
            }
            if(model.has("imageurl"))
            {
                Glide.with(holder.reviewImage.getContext())
                        .load(model.get("imageurl").getAsString())
                        .into(holder.reviewImage);
            }
            if(model.has("realname"))
            {
                holder.realname.setText(model.get("realname").getAsString());
            }
            if(model.has("firstappearance"))
            {
                holder.firstappearance.setText(model.get("firstappearance").getAsString());
            }
            if(model.has("team"))
            {
                holder.team.setText(model.get("team").getAsString());
            }
            if(model.has("publisher"))
            {
                holder.publisher.setText(model.get("publisher").getAsString());
            }
            if(model.has("bio"))
            {
                holder.bio.setText(model.get("bio").getAsString());
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }
    @Override
    public int getItemCount() {
        return jArray.size();
    }
    public class JsonHolder extends RecyclerView.ViewHolder
    {
        private TextView name,realname,team,firstappearance,publisher,bio,createdBy;
        private ImageView reviewImage;
        public JsonHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.jsonName);
            createdBy=itemView.findViewById(R.id.createdBy);
            realname=itemView.findViewById(R.id.realname);
            team=itemView.findViewById(R.id.team);
            firstappearance=itemView.findViewById(R.id.firstAppearence);
            publisher=itemView.findViewById(R.id.publisher);
            bio=itemView.findViewById(R.id.bio);
            reviewImage=itemView.findViewById(R.id.imageJson);
        }
    }
}

