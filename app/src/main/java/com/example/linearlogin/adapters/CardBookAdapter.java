package com.example.linearlogin.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.model.CardBookModel;

import java.util.List;

public class CardBookAdapter extends RecyclerView.Adapter<CardBookAdapter.CardHolder> {
    private List<CardBookModel> cardBook;
    public CardBookAdapter(List<CardBookModel> cardBook) {
        this.cardBook = cardBook;
    }
    @NonNull
    @Override
    public CardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.continue_reading,parent,false);
        return new CardHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull CardBookAdapter.CardHolder holder, int position) {
    CardBookModel model=cardBook.get(position);
    holder.cardAuthor.setText(model.getCardAuthor());
    holder.cardTitle.setText(model.getCardTitle());
        Glide.with(holder.cardImageBook.getContext())
                .load(model.getCardImage())
                .into(holder.cardImageBook);

    }
    @Override
    public int getItemCount() {
        return cardBook.size();
    }
    public class CardHolder extends RecyclerView.ViewHolder {
        private TextView cardAuthor, cardTitle;
        private ImageView cardImageBook;

        public CardHolder(@NonNull View itemView) {
            super(itemView);
            cardAuthor = (TextView) itemView.findViewById(R.id.bookAuthorCard);
            cardTitle = (TextView) itemView.findViewById(R.id.bookName);
            cardImageBook = itemView.findViewById(R.id.bookcardImg);
        }
}
}