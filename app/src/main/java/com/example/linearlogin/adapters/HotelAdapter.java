package com.example.linearlogin.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.model.ReviewHotelModel;

import java.text.BreakIterator;
import java.util.List;

public class HotelAdapter extends RecyclerView.Adapter<HotelAdapter.ViewHolder> {
private List<ReviewHotelModel> reviewModels;

    public HotelAdapter(List<ReviewHotelModel> reviewModels) {
        this.reviewModels = reviewModels;
       // this.calender = calender;
    }



@NonNull
@Override
public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.hotel_list_recyler,parent,false);
        return new ViewHolder(view);
        }

@Override
public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    ReviewHotelModel model = reviewModels.get(position);
    holder.name.setText(model.getName());
    holder.place.setText(model.getPlace());
    Glide.with(holder.hotelImage.getContext())
            .load(model.getHotelImage())
            .into(holder.hotelImage);
    holder.points.setText(model.getPoints());
    holder.buttons.setText(model.getButtons());

}

@Override
public int getItemCount() {
        return reviewModels.size();
        }
public class ViewHolder extends RecyclerView.ViewHolder
{
    private TextView name,place,points;
    private ImageView hotelImage;
    private Button buttons;
    public ViewHolder(@NonNull View itemView) {
        super(itemView);
      name=itemView.findViewById(R.id.hotelName);
        place=itemView.findViewById(R.id.hotelText);
        hotelImage=itemView.findViewById(R.id.hotelImg);
        points=itemView.findViewById(R.id.starsPoint);
        buttons=itemView.findViewById(R.id.buttonBook);

    }
}

}

