package com.example.linearlogin.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.linearlogin.movies.fragments.AvialableFragment;
import com.example.linearlogin.movies.fragments.UpcomingMovie;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private final int tabCount;

    public ViewPagerAdapter(@NonNull FragmentManager fm, int tabCount) {
        super(fm, tabCount);
        this.tabCount=tabCount;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                AvialableFragment avialableFragment=new AvialableFragment();
                return avialableFragment;
            case 1:
                UpcomingMovie upcomingMovie=new UpcomingMovie();
                return upcomingMovie;
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
