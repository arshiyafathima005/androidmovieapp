package com.example.linearlogin.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.model.ReviewHotelModel;

import java.util.List;

public class GridHotelAdapter extends RecyclerView.Adapter<GridHotelAdapter.GridHolder> {
    private List<ReviewHotelModel> grid;

    public GridHotelAdapter(List<ReviewHotelModel> grid) {
        this.grid = grid;
    }

    @NonNull
    @Override
    public GridHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.gird_hotel, parent, false);
        return new GridHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GridHolder holder, int position) {
        ReviewHotelModel model = grid.get(position);
        holder.topText.setText(model.getOffer());
        holder.bottomText.setText(model.getDestination());
        Glide.with(holder.image.getContext())
                .load(model.getImage())
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return grid.size();
    }

    public class GridHolder extends RecyclerView.ViewHolder {
        private TextView topText, bottomText;
        private ImageView image;

        public GridHolder(@NonNull View itemView) {
            super(itemView);
            topText = (TextView) itemView.findViewById(R.id.topText);
            bottomText = (TextView) itemView.findViewById(R.id.bottomText);
            image = itemView.findViewById(R.id.image);
        }
    }
}
