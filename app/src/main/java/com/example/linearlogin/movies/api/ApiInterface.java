package com.example.linearlogin.movies.api;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    //base url or root url
    String BASE_URL = "https://simplifiedcoding.net/demos/";
    //define request to appln name or root
    @GET("marvel")
        //define a method whose return type is call and also define type of the call.
        //type of the call is we are getting jsonArray data.
    Call <JsonArray> getMovieList();
    //Call<List<MovieModel>> getMovieList();

}