package com.example.linearlogin.movies.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    //private using within class static-value is shared by all objects of this class
    private static RetrofitClient instance = null;
    private ApiInterface myApi;
    //intialize the object to desired values or default when object is created.
    private RetrofitClient() {
        //builder build a new retrofit and baseurl()-used to set base url
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiInterface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())//converts json object to java object
                .build();//build our retrofit object
        myApi = retrofit.create(ApiInterface.class);//creates an implementaion of api based on service method i,e GET,POST etc.
    }

    public static synchronized RetrofitClient getInstance() {
        if (instance == null) {
            instance = new RetrofitClient();
        }
        return instance;
    }

    public ApiInterface getMyApi() {
        return myApi;
    }
}
