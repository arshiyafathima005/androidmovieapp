package com.example.linearlogin.movies.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.linearlogin.R;
import com.example.linearlogin.adapters.ReviewAdapter;
import com.example.linearlogin.model.ReviewModel;

import java.util.ArrayList;
import java.util.List;


public class UpcomingMovie extends Fragment {
    private RecyclerView recyclerv;
    private final List<ReviewModel> reviews = new ArrayList<>();
    private ReviewAdapter adapter;

    public UpcomingMovie() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_upcoming_movie, container, false);
        recyclerv = rootView.findViewById(R.id.recycler1);
        recyclerv.setLayoutManager(new LinearLayoutManager(getActivity()));
        populateData();
        return rootView;
    }

    private void populateData() {
        ReviewModel reviewModel1 = new ReviewModel(" Brahamastra Tovino thomas And Reba monica Are Amazing,Brilliant,and fantastic.", "12-12-2020", "https://sm.mashable.com/mashable_in/seo/1/11730/11730_swqe.jpg");
        ReviewModel reviewModel2 = new ReviewModel("83 is Amazing upcoming movie ", "11-12-2020", "https://sm.mashable.com/t/mashable_in/photo/default/83-first-look-ranveer-singh-kamal-haasan-kapil-dev-unveil-po_aa81.960.jpg");
        ReviewModel reviewModel3 = new ReviewModel("Eternals is Super Amazing,Brilliant,and fantastic with their Powerful", "12-12-2020", "https://i2.wp.com/honknews.com/wp-content/uploads/2020/04/The-Eternals-Cast-Plot-and-Release-Date.jpg?fit=1080%2C1080&ssl=1");
        ReviewModel reviewModel4 = new ReviewModel("Jamie is Strong power packed performances", "13-12-2020", "https://upload.wikimedia.org/wikipedia/en/c/c6/Everybody%27s_Talking_About_Jamie_%28film%29.png");
        reviews.add(reviewModel1);
        reviews.add(reviewModel2);
        reviews.add(reviewModel3);
        reviews.add(reviewModel4);

        // adapter=new ReviewAdapter(reviews);
        recyclerv.setAdapter(adapter);


    }
}