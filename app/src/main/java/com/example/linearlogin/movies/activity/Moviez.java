package com.example.linearlogin.movies.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import com.example.linearlogin.adapters.ViewPagerAdapter;
import com.example.linearlogin.R;
import com.example.linearlogin.databinding.ActivityMoviezBinding;
import com.google.android.material.tabs.TabLayout;

public class Moviez extends AppCompatActivity {
    private ActivityMoviezBinding binding;
    private ViewPagerAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_moviez);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_moviez);
        adapter=new ViewPagerAdapter(getSupportFragmentManager(),2);
        binding.viewPager.setAdapter(adapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);//to set tab wirh VP
        binding.tabLayout.getTabAt(0).setText("Available");
        binding.tabLayout.getTabAt(1).setText("Upcoming");
        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.viewPager.setCurrentItem(tab.getPosition());//to set VP

              /*  if (tab.getPosition() == 0) {
                    AvialableFragment frag = new AvialableFragment();
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    transaction.add(R.id.frameContainer, frag);
                    transaction.commit();
                } else if (tab.getPosition() == 1) {
                    UpcomingMovie frag = new UpcomingMovie();
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    transaction.replace(R.id.frameContainer, frag);
                    transaction.commit();
                }*/

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }
}