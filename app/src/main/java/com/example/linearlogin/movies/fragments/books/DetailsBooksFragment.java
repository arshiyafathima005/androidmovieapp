package com.example.linearlogin.movies.fragments.books;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.activity.BooksActivity;
import com.example.linearlogin.databinding.FragmentDetailsBooksBinding;
public class DetailsBooksFragment extends Fragment {
    private FragmentDetailsBooksBinding binding;
    public DetailsBooksFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details_books, container, false);
        ((BooksActivity)getActivity()).toolbar().setTitle("Details Book");
        Glide.with(this)
                .load("https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQsyDjvKNiDCsLmkUrRbIFbeHSL-sV5JFsJm2ez7iMbn5vmZbfr")
                .into(binding.imgKiteRunner);

        binding.btnAddcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.moreBookFragment);

            }

        });
        return binding.getRoot();
    }

}