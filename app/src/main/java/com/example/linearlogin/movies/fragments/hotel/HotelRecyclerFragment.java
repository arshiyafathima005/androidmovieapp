package com.example.linearlogin.movies.fragments.hotel;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.linearlogin.R;
import com.example.linearlogin.adapters.CalenderAdapter;
import com.example.linearlogin.adapters.HotelAdapter;
import com.example.linearlogin.databinding.FragmentHotelRecyclerBinding;
import com.example.linearlogin.model.ReviewHotelModel;

import java.util.ArrayList;
import java.util.List;

public class HotelRecyclerFragment extends Fragment {
    private FragmentHotelRecyclerBinding binding;
    private List<ReviewHotelModel> reviews = new ArrayList<>();
    private HotelAdapter adapter;
   private CalenderAdapter calenderAdapter;

    public HotelRecyclerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_hotel_recycler, container, false);
        /*Glide.with(this)
                .load("https://i2.wp.com/static.web-backgrounds.net/uploads/2012/08/Abstract_Green_Background_with_Floral_Elements.jpg")
                .into(binding.area);*/
        populateData();
        calData();
        return binding.getRoot();
    }

    private void populateData() {
        ReviewHotelModel reviewModel1 = new ReviewHotelModel("Hotel Paradies", "paris,india", "https://www.ahstatic.com/photos/9399_ho_00_p_1024x768.jpg","4.4","Book");
        ReviewHotelModel reviewModel2 = new ReviewHotelModel("Hotel Goodie", "india,usa", "https://imgcy.trivago.com/c_lfill,d_dummy.jpeg,e_sharpen:60,f_auto,h_450,q_auto,w_450/itemimages/11/05/1105078_v4.jpeg","4.2","Book");
        ReviewHotelModel reviewModel3 = new ReviewHotelModel("Hotel Hoddie", "bangalore", "https://badianhotel.com/wp-content/uploads/2020/08/baglioni-hotel-london.jpg","4.4","Book");
        ReviewHotelModel reviewModel4 = new ReviewHotelModel("Hotel Jeevan", "mumbai", "https://images.moneycontrol.com/static-mcnews/2019/07/Indian-hotels-770x433.jpg?impolicy=website&width=770&height=431","4.3","Book");
        reviews.add(reviewModel1);
        reviews.add(reviewModel2);
        reviews.add(reviewModel3);
        reviews.add(reviewModel4);
        adapter = new HotelAdapter(reviews);
        binding.reviewHotel.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.reviewHotel.setAdapter(adapter);
       //
    }
    private void calData()
    {
        ReviewHotelModel calender1=new ReviewHotelModel("SAT","08");
        ReviewHotelModel calender2=new ReviewHotelModel("SUN","09");
        ReviewHotelModel calender3=new ReviewHotelModel("MON","10");
        ReviewHotelModel calender4=new ReviewHotelModel("TUES","11");
        ReviewHotelModel calender5=new ReviewHotelModel("WED","12");
        ReviewHotelModel calender6=new ReviewHotelModel("THUR","13");
        ReviewHotelModel calender7=new ReviewHotelModel("FRI","14");
        reviews.add(calender1);
        reviews.add(calender2);
        reviews.add(calender3);
        reviews.add(calender4);
        reviews.add(calender5);
        reviews.add(calender6);
        reviews.add(calender7);
        calenderAdapter=new CalenderAdapter(reviews);
        binding.calender.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,true));
        binding.calender.setAdapter(calenderAdapter);
    }
}