package com.example.linearlogin.movies.fragments.hotel;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.adapters.GridHotelAdapter;
import com.example.linearlogin.adapters.HotelAdapter;
import com.example.linearlogin.databinding.FragmentHotelGridBinding;
import com.example.linearlogin.model.ReviewHotelModel;

import java.util.ArrayList;
import java.util.List;

public class HotelGridFragment extends Fragment {
    private FragmentHotelGridBinding binding;
    private List<ReviewHotelModel> reviews = new ArrayList<>();
    private GridHotelAdapter adapter;
    public HotelGridFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_hotel_grid, container, false);
        Glide.with(this)
                .load("https://rachelehphotography.com/wp-content/uploads/2019/10/one_shot_for_linkedin_3-1.jpg")
                .into(binding.userImage);
        populateData();
        return binding.getRoot();

    }
    private void populateData() {
        ReviewHotelModel reviewModel1 = new ReviewHotelModel("jan offer","TOP","https://www.gannett-cdn.com/-mm-/05b227ad5b8ad4e9dcb53af4f31d7fbdb7fa901b/c=0-64-2119-1259/local/-/media/USATODAY/USATODAY/2014/08/13/1407953244000-177513283.jpg?width=2119&height=1195&fit=crop&format=pjpg&auto=webp");
        ReviewHotelModel reviewModel2 = new ReviewHotelModel("jan offer","TOP DESTINATION","https://cf.bstatic.com/images/hotel/max1024x768/475/47500229.jpg");
        ReviewHotelModel reviewModel3 = new ReviewHotelModel("jan offer","TOP DESTINATION","https://u.profitroom.pl/2018-zimnik-com-pl/thumb/1920x1080/uploads/Wnetrza_pokoje/Hotel_zimnik_465.jpg");
        ReviewHotelModel reviewModel4 = new ReviewHotelModel("jan offer","TOP DESTINATION","https://content.r9cdn.net/res/images/marble/seo_hotels.jpg?v=aeb8c67f83d5b9fd53ca97055fc8402800bf3ce4&cluster=4");
        reviews.add(reviewModel1);
        reviews.add(reviewModel2);
        reviews.add(reviewModel3);
        reviews.add(reviewModel4);
        adapter = new GridHotelAdapter(reviews);
        binding.gridRecycler.setLayoutManager(new GridLayoutManager(getActivity(),2));
        binding.gridRecycler.setAdapter(adapter);
    }

}