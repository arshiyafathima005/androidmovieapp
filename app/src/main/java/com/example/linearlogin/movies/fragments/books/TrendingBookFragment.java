package com.example.linearlogin.movies.fragments.books;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.activity.BooksActivity;
import com.example.linearlogin.adapters.BookAdapter;
import com.example.linearlogin.adapters.CardBookAdapter;
import com.example.linearlogin.databinding.FragmentTrendingBookBinding;
import com.example.linearlogin.model.BookModel;
import com.example.linearlogin.model.CardBookModel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
public class TrendingBookFragment extends Fragment {
    private final List<BookModel> books = new ArrayList<>();
    private final List<CardBookModel> cardBook=new ArrayList<>();
    private FragmentTrendingBookBinding binding;
    private BookAdapter adapter;
    private CardBookAdapter cardAdapter;
    public TrendingBookFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_trending_book, container, false);
        ((BooksActivity)getActivity()).toolbar().setTitle("Trending Books");
       /* Glide.with(this)
                .load("https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mnx8d29tYW58ZW58MHx8MHw%3D&ixlib=rb-1.2.1&w=1000&q=80")
                .into(binding.userEnd);*/
       /* Glide.with(this)
                .load("https://images.unsplash.com/photo-1541963463532-d68292c34b19?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max")
                .into(binding.bookcardImg);*/
        populateData();
        populateCard();
        binding.continueReading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.bookDetailsFragment);
            }
        });
        return binding.getRoot();
    }
    private void populateData() {
        BookModel bookModel1 = new BookModel("by Whiteny", "Read to Success", "https://m.media-amazon.com/images/I/51RWzpMasQL.jpg");
        BookModel bookModel2 = new BookModel("by Joseph", "Believe in YourSelf ", "https://prodigal.typepad.com/.a/6a00d8345192a569e201b8d2071a51970c-800wi");
        BookModel bookModel3 = new BookModel("by Whiteny D", "A Love Hate Thing", "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQsyDjvKNiDCsLmkUrRbIFbeHSL-sV5JFsJm2ez7iMbn5vmZbfr");
        books.add(bookModel1);
        books.add(bookModel2);
        books.add(bookModel3);
        adapter = new BookAdapter(books);
        binding.bookRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));
        binding.bookRecycler.setAdapter(adapter);
    }
    private void populateCard()
    {
         CardBookModel cardBookModel1=new CardBookModel("by Whiteny","Read To Success","https://m.media-amazon.com/images/I/51RWzpMasQL.jpg");
         CardBookModel cardBookModel2=new CardBookModel("by Joseph","Big Magic","https://prodigal.typepad.com/.a/6a00d8345192a569e201b8d2071a51970c-800wi");
         CardBookModel cardBookModel3=new CardBookModel("by Whiteny","A Love Hate Thing","https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQsyDjvKNiDCsLmkUrRbIFbeHSL-sV5JFsJm2ez7iMbn5vmZbfr");
         cardBook.add(cardBookModel1);
         cardBook.add(cardBookModel2);
         cardBook.add(cardBookModel3);
         cardAdapter=new CardBookAdapter(cardBook);
         binding.cardbookRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
         binding.cardbookRecycler.setAdapter(cardAdapter);
    }

}