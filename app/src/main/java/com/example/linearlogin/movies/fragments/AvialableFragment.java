package com.example.linearlogin.movies.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.linearlogin.adapters.ReviewAdapter;
import com.example.linearlogin.R;
import com.example.linearlogin.databinding.FragmentAvialableBinding;
import com.example.linearlogin.model.ReviewModel;
import com.example.linearlogin.movies.activity.MoviePoster;
import com.example.linearlogin.movies.activity.SharedPrefferenceConfig;
import com.example.linearlogin.movies.api.MovieModel;
import com.example.linearlogin.movies.api.RetrofitClient;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AvialableFragment extends Fragment {

    private List<MovieModel> reviews=new ArrayList<>();
    private ReviewAdapter adapter;
    private FragmentAvialableBinding binding;
    private static final String TAG="MoviePoster";
    public AvialableFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
        @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

            binding= DataBindingUtil.inflate(inflater,R.layout.fragment_avialable,container,false);
           // getApiData();

            return binding.getRoot();
    }

    /*private void getApiData()
    {
        Call<List<MovieModel>> call = RetrofitClient.getInstance().getMyApi().getMovieList();

        //to perform the API call we need to call the method enqueue()
        //We need to pass a Callback with enqueue method
        //And Inside the callback functions we will handle success or failure of
        //the result that we got after the API call
        call.enqueue(new Callback<List<MovieModel>>() {
            @Override
            public void onResponse(Call<List<MovieModel>> call, Response<List<MovieModel>> response) {

                //In this point we got our movie list
                //thats damn easy right ;)
                List<MovieModel> movieModels = response.body();
                Log.d(TAG, "onResponse: " + new Gson().toJson(movieModels));
                adapter=new ReviewAdapter(movieModels);
                binding.recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
              binding.recycler.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<List<MovieModel>> call, Throwable t) {
                //handle error or failure cases here
                Log.e("MoviePoster",""+t.getMessage());
            }
        });


    }*/
}