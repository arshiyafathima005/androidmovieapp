package com.example.linearlogin.movies.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.linearlogin.adapters.ReviewAdapter;
import com.example.linearlogin.databinding.ActivityMoviePosterBinding;
import com.example.linearlogin.movies.api.RetrofitClient;
import com.example.linearlogin.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.linearlogin.movies.api.ApiInterface.BASE_URL;

public class MoviePoster extends AppCompatActivity {
    private RecyclerView recyclerView;
    private List<JsonObject> reviews=new ArrayList<>();
    private ReviewAdapter adapter;
    JsonObject jsonObject;
    private ActivityMoviePosterBinding binding;
    private static final String TAG="MoviePoster";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_movie_poster);
        recyclerView=binding.recyclerView;
        Glide.with(this)
                .load("https://i.pinimg.com/originals/3e/a1/37/3ea1371e08a4634823d8fbfa2ae14f65.jpg")
                .into(binding.imageView);
       // set();
        getApiData();
    }
    private void getApiData() {
        //Call<List<MovieModel>> call = RetrofitClient.getInstance().getMyApi().getMovieList();
        Call<JsonArray> call = RetrofitClient.getInstance().getMyApi().getMovieList();
        Log.d(TAG, "getApiData: "+call);
        //to perform the API call we need to call the method enqueue()
        //We need to pass a Callback with enqueue method
        //And Inside the callback functions we will handle success or failure of
        //the result that we got after the API call
        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                //In this point we got our hero list
                //thats damn easy right ;)
                JsonArray movieModels = response.body();
                Log.d(TAG, "onResponse: " + new Gson().toJson(movieModels));
                adapter = new ReviewAdapter(movieModels);
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                recyclerView.setAdapter(adapter);
                //now we can do whatever we want with this list
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                //handle error or failure cases here
                Log.e("MoviePoster", "" + t.getMessage());
            }
        });
    }
}