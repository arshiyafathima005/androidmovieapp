package com.example.linearlogin.movies.fragments.books;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.activity.BooksActivity;
import com.example.linearlogin.adapters.BookAdapter;
import com.example.linearlogin.databinding.FragmentMoreBookBinding;
import com.example.linearlogin.model.BookModel;

import java.util.ArrayList;
import java.util.List;
public class MoreBookFragment extends Fragment {
    private FragmentMoreBookBinding binding;
    private final List<BookModel> book = new ArrayList<>();
    private BookAdapter bookAdapter;
    public MoreBookFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_more_book,container,false);
        ((BooksActivity)getActivity()).toolbar().setTitle("More Books");
        Glide.with(this)
                .load("https://images-na.ssl-images-amazon.com/images/I/916oR7ry1tL.jpg")
                .into(binding.imgKite);
        populatedData();
        return binding.getRoot();
    }
    private void populatedData() {
        BookModel bookModel1 = new BookModel("Dominic Lopez", "Harry Potter", "https://images-na.ssl-images-amazon.com/images/I/51mIn6jlDHL.jpg");
        BookModel bookModel2 = new BookModel("Whitney D.Grandison", "A love Hate Thing", "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQsyDjvKNiDCsLmkUrRbIFbeHSL-sV5JFsJm2ez7iMbn5vmZbfr");
        BookModel bookModel3 = new BookModel("Jeff Vande", "Read to Success", "https://m.media-amazon.com/images/I/51RWzpMasQL.jpg");
        book.add(bookModel1);
        book.add(bookModel2);
        book.add(bookModel3);
        bookAdapter = new BookAdapter(book);
        binding.recylerMore.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));
        binding.recylerMore.setAdapter(bookAdapter);
    }
}