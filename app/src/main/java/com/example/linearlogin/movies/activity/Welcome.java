package com.example.linearlogin.movies.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.linearlogin.R;
import com.example.linearlogin.databinding.ActivityWelcomeBinding;

public class Welcome extends AppCompatActivity {
    private ActivityWelcomeBinding binding;
    private SharedPrefferenceConfig sharedPrefferenceConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_welcome);
        sharedPrefferenceConfig = new SharedPrefferenceConfig(this);
        binding.gmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPrefferenceConfig.storeUserName("Arshiya");
                Intent gmailIntent = new Intent(getApplicationContext(), Moviez.class);
                startActivity(gmailIntent);
                finish();//it will not go to the previous activity when we press back button directly off
            }
        });
        binding.facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent facebookIntent = new Intent(getApplicationContext(), Moviez.class);
                startActivity(facebookIntent);
            }
        });

        if (!sharedPrefferenceConfig.getUserName().equals("")) {
            Intent myIntent = new Intent(getApplicationContext(), Moviez.class);
            startActivity(myIntent);

        } else {
            System.out.println("No data Available");
        }
        // sharedPrefferenceConfig.storeUserName("Arshiya");
    }
}