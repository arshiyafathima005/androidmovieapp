package com.example.linearlogin.movies.activity;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.linearlogin.R;

public class SharedPrefferenceConfig {
    private SharedPreferences sharedPreferences;
    private Context context;

    public SharedPrefferenceConfig(Context context) {
        this.context = context;
        sharedPreferences=context.getSharedPreferences(context.getString(R.string.app_name),Context.MODE_PRIVATE);

    }
    public void storeUserName(String userName)
    {
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("userName",userName);
        editor.apply();
    }
    public String getUserName()
    {
        String userName,email;
        userName=sharedPreferences.getString("userName","");
        return userName;
    }
    public void storeEmail(String email)
    {
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("email",email);
        editor.apply();
    }
    public String getEmail()
    {
        String email;
        email=sharedPreferences.getString("email","");
        return email;
    }
    public void storeAddhar(String addhar)
    {
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("addhar",addhar);
        editor.apply();
    }
    public String getAddhar()
    {
        String addhar;
        addhar=sharedPreferences.getString("addhar","");
        return addhar;
    }
    public void storePan(String pan)
    {
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("pan",pan);
        editor.apply();
    }
    public String getPan()
    {
        String pan;
        pan=sharedPreferences.getString("pan","");
        return pan;
    }
    public void storeTenth(String tenth)
    {
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("tenth",tenth);
        editor.apply();
    }
    public String getTenth()
    {
        String tenth;
        tenth=sharedPreferences.getString("tenth","");
        return tenth;
    }



}
