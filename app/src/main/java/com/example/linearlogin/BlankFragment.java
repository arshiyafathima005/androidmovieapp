package com.example.linearlogin;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;

public class BlankFragment extends Fragment {
    private static final String TAG="BlankFragment";

    public BlankFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.d(TAG,"onAttach is called");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate is called");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_blank, container, false);
        ImageView image = (ImageView)rootView.findViewById(R.id.imageView4);
        Glide.with(this)
                .load("https://html5box.com/html5lightbox/images/mountain.jpg")
                .into(image);
        return  rootView;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG,"onActivity created is called");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG,"onStart is called");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG,"onResume is called");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG,"onPause is called");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG,"ooStop is called");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG,"onDestroyView is called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"onDestroy is called");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG,"onDetach is called");
    }
}