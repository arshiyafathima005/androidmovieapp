package com.example.linearlogin.data_assets_json;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.example.linearlogin.R;
import com.example.linearlogin.databinding.ActivityAssetJsonBinding;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class AssetJsonActivity extends AppCompatActivity {
    private ActivityAssetJsonBinding binding;
    RecyclerView recyclerView;
    /*ArrayList<String> desc=new ArrayList<>();
    ArrayList<String> name=new ArrayList<>();
    ArrayList<String> title=new ArrayList<>();
    ArrayList<String> subtitle=new ArrayList<>();
    ArrayList<String> image=new ArrayList<>();*/
    private List<JSONObject> item=new ArrayList<>();
    private String fileString;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_asset_json);
        recyclerView=binding.assetRecycler;
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        fileString=getJsonFromAssets(getApplicationContext(),"data_asset.json");

        /*try {
            JsonObject jsonObject = new Jsonbject(getJsonFromAssets(getApplicationContext(),"data_asset,json"));
            Log.d("jsonobject", "onCreate: "+jsonObject);
            if(jsonObject.has("categories")) {
                JSONArray jsonArray = jsonObject.getJSONArray("categories");
                if(jsonArray.length()>0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        if(jsonObject1.has("name"))
                        {
                            name.add(jsonObject1.getString("name"));
                        }
                        if(jsonObject1.has("videos"))
                        {
                            JSONArray videosArray=jsonObject1.getJSONArray("video");
                            for(int j=0;j<videosArray.length();j++)
                            {
                                JSONObject videoObject=videosArray.getJSONObject(j);
                                desc.add(videoObject.getString("description"));
                                title.add(videoObject.getString("title"));
                                subtitle.add(videoObject.getString("subtitle"));
                                image.add(videoObject.getString("thumb"));
                            }
                        }
                    }
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }*/
       // AdapterDataAssets adapter=new AdapterDataAssets(item,this);
        //recyclerView.setAdapter(adapter);


    }
    /*public String getJsonFromAssets() {
        String jsonString=null;
        try {
            InputStream is = getAssets().open("data_asset.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            jsonString = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
           return null;
        }

        return jsonString;
    }*/
    public static String getJsonFromAssets(Context context, String fileName) {
        String jsonString;
        try {
            InputStream is = context.getAssets().open("data_asset.json");

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            jsonString = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return jsonString;
    }
}