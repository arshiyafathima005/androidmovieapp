package com.example.linearlogin.data_assets_json;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.adapters.AdapterJsons;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AdapterDataAssets extends RecyclerView.Adapter<AdapterDataAssets.JsonHolder> {
   /*ArrayList<String> name;
   ArrayList<String> desc;
   ArrayList<String> title;
   ArrayList<String> subtitle;*/
   //ArrayList<String> image;
    private List<JsonObject> item;
   Context context;

    /*public AdapterDataAssets(ArrayList<String> name, ArrayList<String> desc, ArrayList<String> title, ArrayList<String> subtitle, Context context) {
        this.name = name;
        this.desc = desc;
        this.title = title;
        this.subtitle = subtitle;
        this.context = context;
    }*/

    public AdapterDataAssets(List<JsonObject> item, Context context) {
        this.item = item;
        this.context = context;
    }

    /*  public AdapterDataAssets(List<JSONObject> item, Context context) {
                this.item = item;
                this.context = context;
            }*/
    @NonNull
    @Override
    public JsonHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.custom_data_assests_json, parent, false);
        return new JsonHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JsonHolder holder, int position) {
            JsonObject jsonObject=item.get(position).getAsJsonObject();


     /* holder.name.setText(name.get(position));
      holder.desc.setText(desc.get(position));
      holder.title.setText(title.get(position));
      holder.subtitle.setText(subtitle.get(position));
        //Picasso.get().load(image).into(holder.image);*/
    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    public class JsonHolder extends RecyclerView.ViewHolder {
        private TextView name,title,subtitle,desc;
       // private ImageView image;

        public JsonHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.movieName);
            title = itemView.findViewById(R.id.titleAssets);
            subtitle = itemView.findViewById(R.id.subtitleAssets);
            desc = itemView.findViewById(R.id.descriptionAssets);
           // image = itemView.findViewById(R.id.firstAppearence);
        }
    }
}

