package com.example.linearlogin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import android.os.Bundle;
import android.view.View;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.adapters.RecylerDynamicAdapter;
import com.example.linearlogin.databinding.ActivityDynamicMenuBinding;
import com.example.linearlogin.model.RecyclerDynamicModel;
import java.util.ArrayList;
import java.util.List;

public class DynamicMenuActivity extends AppCompatActivity {
private ActivityDynamicMenuBinding binding;
    private List<RecyclerDynamicModel> reviews = new ArrayList<>();
    private List<RecyclerDynamicModel> ibranch=new ArrayList<>();
    private List<RecyclerDynamicModel> ienquiry=new ArrayList<>();
    private List<RecyclerDynamicModel> iadmin=new ArrayList<>();
    private RecylerDynamicAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_dynamic_menu);
        Glide.with(this)
                .load("https://cdn1.vectorstock.com/i/1000x1000/96/95/autumn-decoration-color-background-with-yellow-vector-21809695.jpg")
                .into(binding.top);
        populated();
        binding.imageUserFirst1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.setData(reviews);
            }
        });
        binding.imageUserSecond1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.setData(ibranch);
            }
        });
        binding.imageUserThird1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.setData(ienquiry);
            }
        });
        binding.imageUserFourth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.setData(iadmin);

            }
        });
    }

    private void populated() {
        RecyclerDynamicModel rev=new RecyclerDynamicModel("Iteller");
        RecyclerDynamicModel rev2=new RecyclerDynamicModel("iBranch");
        RecyclerDynamicModel rev3=new RecyclerDynamicModel("iAdmin");
        RecyclerDynamicModel reviewModel1 = new RecyclerDynamicModel("Cash Deposite","https://cf.bstatic.com/images/hotel/max1024x768/475/47500229.jpg");
        RecyclerDynamicModel reviewModel2 = new RecyclerDynamicModel("Cash Withdraw","https://www.dqindia.com/wp-content/uploads/2015/10/Bank-Nationalization-Hindi.jpg");
        RecyclerDynamicModel reviewModel3 = new RecyclerDynamicModel("Fund Transfer","https://ie.binus.ac.id/files/2015/09/banking1.jpg");
        RecyclerDynamicModel reviewModel4 = new RecyclerDynamicModel("Third Party Cash Deposite","https://www.thebalance.com/thmb/QKdak2jvsD1uGh9AxoLvJ9WViAw=/1043x1006/filters:fill(auto,1)/Credit-Card-Bank-Account-by-lvcandy-Getty-488389053-56a1c27c5f9b58b7d0c25749.jpg");

        reviews.add(reviewModel1);
        reviews.add(reviewModel2);
        reviews.add(reviewModel3);
        reviews.add(reviewModel4);

        ibranch.add(reviewModel1);
        ibranch.add(reviewModel2);
        ienquiry.add(reviewModel3);

        iadmin.add(reviewModel1);
        iadmin.add(reviewModel3);
        iadmin.add(reviewModel4);
        iadmin.add(reviewModel3);
        iadmin.add(reviewModel4);
        iadmin.add(reviewModel3);
        iadmin.add(reviewModel4);
        adapter = new RecylerDynamicAdapter(reviews);
        binding.dynamicMenu.setLayoutManager(new GridLayoutManager(getApplicationContext(),4));
        binding.dynamicMenu.setAdapter(adapter);
    }
   // RecyclerDynamicModel iadmin1 = new RecyclerDynamicModel("Cash Deposite","https://cf.bstatic.com/images/hotel/max1024x768/475/47500229.jpg");

}