package com.example.linearlogin.activity;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.example.linearlogin.R;
import com.example.linearlogin.databinding.ActivityBooksBinding;
public class BooksActivity extends AppCompatActivity {
    private ActivityBooksBinding binding;
    private NavController navController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_books);
        setUpNavigation();
        setSupportActionBar(binding.toolBarBooks);
    }
    private void setUpNavigation() {
        try {
            navController = Navigation.findNavController(this, R.id.bookHost);
            NavigationUI.setupWithNavController(binding.bottomBookMenu, navController);
            //Set the tint which is applied to our menu items' icons.
            binding.bottomBookMenu.setItemIconTintList(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onSupportNavigateUp() {
        return Navigation.findNavController(this, R.id.bookHost).navigateUp();
    }
    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.book_toolbar,menu);
        if(menu instanceof MenuBuilder){
            MenuBuilder menuBuilder = (MenuBuilder) menu;
            menuBuilder.setOptionalIconsVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==R.id.exploreBook)
        {
            Navigation.findNavController(this,R.id.bookHost).navigate(R.id.trendingBookFragment);
        } else if (item.getItemId()==R.id.readingBook) {
            Navigation.findNavController(this,R.id.bookHost).navigate(R.id.bookDetailsFragment);
        }
        else if (item.getItemId()==R.id.moreBook) {
            Navigation.findNavController(this,R.id.bookHost).navigate(R.id.moreBookFragment);
        }

        return super.onOptionsItemSelected(item);
    }
    public Toolbar toolbar()
    {
        return binding.toolBarBooks;
    }

}