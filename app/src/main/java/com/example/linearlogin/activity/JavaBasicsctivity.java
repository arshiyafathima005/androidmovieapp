package com.example.linearlogin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.linearlogin.R;
import com.example.linearlogin.databinding.ActivityJavaBasicsctivityBinding;

public class JavaBasicsctivity extends AppCompatActivity {
    private ActivityJavaBasicsctivityBinding binding;
   private int result;

   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_java_basicsctivity);
      // if(a!="" && b!="") {
           binding.sum.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   result = sum();
                   //Log.d("sum","of a and b"+result);
                   Toast.makeText(getApplicationContext(), "sum=" + result, Toast.LENGTH_SHORT).show();
               }
           });
           binding.sub.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   result = sub();
                   //Log.d("sum","of a and b"+result);
                   Toast.makeText(getApplicationContext(), "difference=" + result, Toast.LENGTH_SHORT).show();
               }
           });
           binding.multiply.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   result = mul();
                   //Log.d("sum","of a and b"+result);
                   Toast.makeText(getApplicationContext(), "multiple=" + result, Toast.LENGTH_SHORT).show();
               }
           });
           binding.divide.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   result = div();
                   //Log.d("sum","of a and b"+result);
                   Toast.makeText(getApplicationContext(), "quotient=" + result, Toast.LENGTH_SHORT).show();
               }
           });
      /* }
       else
       {
           Toast.makeText(getApplicationContext(), "enter proper output",Toast.LENGTH_SHORT).show();
       }*/

}
         /*if(a!=0||b!=0){}
         if(s1.length()>=10 && s1.length()<=100){}
         a++;
         ++a;*/
    //Add to numbers
    private int sum()
     {  String a=binding.Value1.getText().toString();
         String b=binding.Value2.getText().toString();

         if(a.length()==0 && b.length()==0)
         {
             Toast.makeText(getApplicationContext(), "enter proper input", Toast.LENGTH_SHORT).show();
        return 0;
         }
             int v1 = Integer.valueOf(a);
             int v2 = Integer.valueOf(b);
         return v1+v2;
     }
    private int sub()
    {  String a=binding.Value1.getText().toString();
       String b=binding.Value2.getText().toString();
        if(a.equals("") && b.equals(""))
        {
            Toast.makeText(getApplicationContext(), "enter proper input", Toast.LENGTH_SHORT).show();
            return 0;
        }
        int v1=Integer.valueOf(a);
        int v2=Integer.valueOf(b);
        return v1-v2;
    }
    private int mul()
    { String a=binding.Value1.getText().toString();
      String b=binding.Value2.getText().toString();
        if(a.length()==0 && b.length()==0)
        {
            Toast.makeText(getApplicationContext(), "enter proper input", Toast.LENGTH_SHORT).show();
            return 0;
        }
        int v1=Integer.valueOf(a);
        int v2=Integer.valueOf(b);
        return v1*v2;
    }
    private int div()
    {  String a=binding.Value1.getText().toString();
        String b=binding.Value2.getText().toString();
        if(a.length()==0 && b.length()==0)
        {
            Toast.makeText(getApplicationContext(), "enter proper input", Toast.LENGTH_SHORT).show();
            return 0;
        }
        int v1=Integer.valueOf(a);
        int v2=Integer.valueOf(b);
        return v1/v2;
    }


}