package com.example.linearlogin.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.ActivityNavigator;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.linearlogin.R;
import com.example.linearlogin.databinding.ActivityNavigateBinding;
import com.example.linearlogin.navigationProject.ProfileFragment;
import com.example.linearlogin.navigationProject.ReviewFragment;

public class NavigationActivity extends AppCompatActivity  {
    private ActivityNavigateBinding binding;
    private NavController navController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_navigate_);
        setUpNavigation();

    }

    private void setUpNavigation() {
        try {
            navController= Navigation.findNavController(this,R.id.hostFragment);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        return Navigation.findNavController(this,R.id.hostFragment).navigateUp();
    }

}