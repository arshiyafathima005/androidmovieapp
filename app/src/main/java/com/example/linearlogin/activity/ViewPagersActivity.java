package com.example.linearlogin.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.view.View;

import com.example.linearlogin.R;
import com.example.linearlogin.adapters.ViewPgersTwoAdapter;
import com.example.linearlogin.databinding.ActivityViewPagersBinding;
import com.example.linearlogin.model.ModelViewPager;

import java.util.ArrayList;
import java.util.List;

public class ViewPagersActivity extends AppCompatActivity {
private ActivityViewPagersBinding binding;
private List<ModelViewPager> loc=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_pagers);
        ModelViewPager model1=new ModelViewPager();
        model1.image="https://www.honeymoonbug.com/blog/wp-content/uploads/2019/12/Kerala.jpg";
        model1.name="Kerala";
        model1.description="Peace full place";
        ModelViewPager model2=new ModelViewPager();
        model2.image="https://i1.wp.com/theluxurytravelexpert.com/wp-content/uploads/2019/04/BEST-BEACH-DESTINATIONS-IN-THE-WORLD.jpg?fit=1300%2C731&ssl=1";
        model2.name="Beach";
        model2.description="Awesome place";
        ModelViewPager model3=new ModelViewPager();
        model3.image="https://www.protraveloholic.com/wp-content/uploads/2019/05/featured-image-1.jpg";
        model3.name="Heaven Mountain";
        model3.description="Really a heaven";
        loc.add(model1);
        loc.add(model2);
        loc.add(model3);
        //set view Pager
        binding.viewpagerTwo.setAdapter(new ViewPgersTwoAdapter(loc));
        binding.viewpagerTwo.setClipToPadding(false);
        binding.viewpagerTwo.setClipChildren(false);
        binding.viewpagerTwo.setPadding(120,0,120,0);
        binding.viewpagerTwo.setOffscreenPageLimit(3);
        binding.viewpagerTwo.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

        CompositePageTransformer compositePageTransformer=new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(40));
        compositePageTransformer.addTransformer(new ViewPager2.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                float p=1-Math.abs(position);
                page.setScaleY(0.95f+p*0.05f);
            }
        });

        binding.viewpagerTwo.setPageTransformer(compositePageTransformer);

    }
}