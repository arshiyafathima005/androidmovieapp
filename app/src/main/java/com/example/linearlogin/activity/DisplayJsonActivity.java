package com.example.linearlogin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.adapters.AdapterJsons;
import com.example.linearlogin.adapters.ReviewAdapter;
import com.example.linearlogin.databinding.ActivityDisplayJsonBinding;
import com.example.linearlogin.movies.api.RetrofitClient;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DisplayJsonActivity extends AppCompatActivity {
    private ActivityDisplayJsonBinding binding;
    private RecyclerView recyclerView;
    private AdapterJsons adapter;
    public DisplayJsonActivity() {
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_display_json);
        recyclerView=binding.jsonRecylerView;
        getApiData();
       }
       private void getApiData()
        {
        //Call<List<MovieModel>> call = RetrofitClient.getInstance().getMyApi().getMovieList();
        Call<JsonArray> call = RetrofitClient.getInstance().getMyApi().getMovieList();

        //to perform the API call we need to call the method enqueue()
        //We need to pass a Callback with enqueue method
        //And Inside the callback functions we will handle success or failure of
        //the result that we got after the API call
        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                //In this point we got our hero list
                JsonArray jsonModels = response.body();
                Log.d("jsonarray", "onResponse: " + new Gson().toJson(jsonModels));
                //now we can do whatever we want with this list
                adapter = new AdapterJsons(jsonModels);
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                recyclerView.setAdapter(adapter);
            }
            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                //handle error or failure cases here
                Log.e("Display Json data",""+t.getMessage());
            }
        });




    }
}