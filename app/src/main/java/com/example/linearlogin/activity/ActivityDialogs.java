package com.example.linearlogin.activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.databinding.ActivityDialogsBinding;
import com.example.linearlogin.databinding.CustomDialogBinding;

public class ActivityDialogs extends AppCompatActivity {
    private ActivityDialogsBinding binding;
    private Dialog dialog;
    private CustomDialogBinding cbinding;
    //private View view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dialogs);
        cbinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.custom_dialog, null, false);
        //view=getLayoutInflater().inflate(R.layout.custom_dialog,null);
        binding.bntDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
    }
    public Dialog showDialog() {
        dialog = new Dialog(ActivityDialogs.this);//
        dialog.setContentView(cbinding.getRoot());//setting custom layout view
        dialog.show();//to show dialog box
        dialog.setCancelable(false);//when we click outside of dialog box then it will not close dialog if set to false
        cbinding.dismissBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();//to cancel dialogbox
            }
        });
        cbinding.allow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Your Allowed In",Toast.LENGTH_SHORT).show();
            }
        });
        cbinding.feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Thank You",Toast.LENGTH_SHORT).show();

            }
        });
        Glide.with(this)
                .load("https://i.pinimg.com/originals/c8/9e/68/c89e6809085102dcfa828d4a165a1399.jpg")
                .into(cbinding.imgDia);
        return dialog;
    }
}