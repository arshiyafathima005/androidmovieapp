package com.example.linearlogin.activity;

import androidx.appcompat.app.AppCompatActivity;
//import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
//import android.util.Log;

import com.example.linearlogin.BlankFragment;
import com.example.linearlogin.R;

public class ActivityLifeCycle extends AppCompatActivity {
   // private static final String TAG="ActivityLifeCycle";
   // private FrameLayout frame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_cycle);
        //Log.d(TAG,"onCreate: called");
        //frame = findViewById(R.id.frame);
        BlankFragment frag = new BlankFragment();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.frame, frag, "Test Fragment");
        transaction.commit();

    }

   /* @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,"onStart is called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"onResume is called");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"onPause is called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,"onStop is called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"onDestroy is called");
    }*/
    }
