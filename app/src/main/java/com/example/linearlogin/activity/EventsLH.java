package com.example.linearlogin.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.linearlogin.R;

public class EventsLH extends AppCompatActivity implements View.OnClickListener  {
    private Button btn,btn1,btn2,btn3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_l_h);
        btn=findViewById(R.id.click);
        btn1=findViewById(R.id.touch);
        btn2=findViewById(R.id.doubleClick);
        btn3=findViewById(R.id.longClick);

        btn.setOnClickListener(this);

        btn1.setOnTouchListener(new View.OnTouchListener() {
          @Override
          public boolean onTouch(View v, MotionEvent event) {
              if(event.getAction()==MotionEvent.ACTION_DOWN)
              {
                  Toast.makeText(getApplicationContext(),"button touched",Toast.LENGTH_SHORT).show();
              }else if(event.getAction()==MotionEvent.ACTION_UP)
              {
                  Toast.makeText(getApplicationContext(),"button action up",Toast.LENGTH_SHORT).show();
              }
              return false;
          }
      });



     btn3.setOnLongClickListener(new View.OnLongClickListener() {
          @Override
          public boolean onLongClick(View v) {
              if(v.getId()==R.id.longClick)
              {
                  Toast.makeText(getApplicationContext(),"Button Long Clicked",Toast.LENGTH_SHORT).show();
              }
              return false;
          }
      });
       
    }

    @Override
    public void onClick(View view)
    {
        if(view.getId()==R.id.click)
        {
            Toast.makeText(getApplicationContext(), "Botton clicked", Toast.LENGTH_SHORT).show();
        }
    }




}
