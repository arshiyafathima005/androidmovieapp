package com.example.linearlogin.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.example.linearlogin.R;

import com.example.linearlogin.databinding.ActivityRecyclerNavigationBinding;
import com.example.linearlogin.movies.fragments.hotel.HotelGridFragment;
import com.example.linearlogin.movies.fragments.hotel.HotelRecyclerFragment;

import java.util.Objects;

public class RecyclerNavigationActivity extends AppCompatActivity {
    private ActivityRecyclerNavigationBinding binding;
    private NavController navController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_recycler_navigation);
        setSupportActionBar(binding.toolBar);
       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpNavigation();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu_item,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==R.id.hotel)
        {
           //getSupportFragmentManager().beginTransaction().
                   // replace(R.id.hotelHost, new HotelRecyclerFragment()).commit();
            Navigation.findNavController(this,R.id.hotelHost).navigate(R.id.hotelRecyclerFragment);
        } else if (item.getItemId()==R.id.gridHotel) {
           // getSupportFragmentManager().beginTransaction().
                   // replace(R.id.hotelHost, new HotelGridFragment()).commit();
        Navigation.findNavController(this,R.id.hotelHost).navigate(R.id.hotelGridFragment);
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpNavigation() {
        try {
            //navController manage the app navigation with navHost
            navController= Navigation.findNavController(this,R.id.hotelHost);
            //for bottom navigation setting up navigation ui with nav controler
            NavigationUI.setupWithNavController(binding.bottomNavRecyclerMenu,navController);
            //Set the tint which is applied to our menu items' icons.
            binding.bottomNavRecyclerMenu.setItemIconTintList(null);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
   /* @Override
   public boolean onSupportNavigateUp()
    {
        return Navigation.findNavController(this,R.id.hotelHost).navigateUp();
    }*/
}
