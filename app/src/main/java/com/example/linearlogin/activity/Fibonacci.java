package com.example.linearlogin.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.example.linearlogin.R;

public class Fibonacci extends AppCompatActivity {
    private static final String TAG = "Fibonacci";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fibonacci);
        printFibonacci();
        palindrome();
    }

    private void printFibonacci() {
         int n=10,num1=0,num2=1;
        Log.d(TAG,"fibonnaci numbers");
        for(int i=1;i<=n;i++)
        {
            Log.d(TAG,+num1+" ");
            int sum=num1+num2;
            num1=num2;
            num2=sum;

        }
    }
    private void palindrome()
    {
        int num=12321,temp = num,rev=0;
        while(num!=0)
        {   rev=(rev*10)+(num%10);
           num=num/10;
        }
        if(temp==rev)
        {
            Log.d(TAG,""+rev+" is palindrome");
        }
        else
        {
            Log.d(TAG,""+rev+" is not palindrome");
        }
    }

}