package com.example.linearlogin.activity.keyframe;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.databinding.DataBindingUtil;

import android.os.Build;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.View;

import com.example.linearlogin.R;


public class KeyFrameActivity extends AppCompatActivity {
  //  private ActivityKeyFrameBinding binding;
    Boolean isShown=false;
    ConstraintLayout layout;
    ConstraintSet constraintSetShow=new ConstraintSet();
    ConstraintSet constraintSetHide=new ConstraintSet();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //binding= DataBindingUtil.
        setContentView(R.layout.activity_key_frame);
       layout= findViewById(R.id.constraintLayout);
        constraintSetHide.clone(layout);
        constraintSetShow.clone(this,R.layout.animation_key_frame);
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void showDetails(View v)
    {
        TransitionManager.beginDelayedTransition(layout);
        if(isShown)
        {
            constraintSetHide.applyTo(layout);//activity layout because false
        }else
        {
            constraintSetShow.applyTo(layout);//animation key inside activity called because ture
        }
        isShown=!isShown;//to perform the animation multiple times like alternate true and false
    }
}