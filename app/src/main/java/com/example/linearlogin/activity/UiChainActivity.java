package com.example.linearlogin.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.linearlogin.R;
import com.example.linearlogin.databinding.ActivityUiChainBinding;

public class UiChainActivity extends AppCompatActivity {
    private ActivityUiChainBinding binding;
    private NavController navController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_ui_chain);
        setSupportActionBar(binding.toolBarChain);
        setUpNavigation();
    }
    private void setUpNavigation() {
        try {
            navController= Navigation.findNavController(this,R.id.chainHost);
            NavigationUI.setupWithNavController(binding.bottomNavigationView,navController);
            binding.bottomNavigationView.setItemIconTintList(null);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onSupportNavigateUp()
    {
        return Navigation.findNavController(this,R.id.chainHost).navigateUp();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chain_toolbar,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==R.id.chainHome)
        {
            Navigation.findNavController(this,R.id.chainHost).navigate(R.id.peopleFragment);
        } else if (item.getItemId()==R.id.chainNext) {
            Navigation.findNavController(this,R.id.chainHost).navigate(R.id.connectFragment);
        }else if(item.getItemId()==R.id.chainLast)
        {
            Navigation.findNavController(this,R.id.chainHost).navigate(R.id.notificationFragment);
        }
        return super.onOptionsItemSelected(item);
    }
}