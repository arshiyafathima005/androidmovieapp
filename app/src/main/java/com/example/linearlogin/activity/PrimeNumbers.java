package com.example.linearlogin.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;

import com.example.linearlogin.R;

import java.util.ArrayList;
import java.util.List;

public class PrimeNumbers extends AppCompatActivity {
    private static final String TAG = "PrimeNumbers";
    private List<Integer> numbers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prime_numbers_main);
        addInList();
        checkPrime();
    }

    private void addInList() {
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(7);
        numbers.add(11);
    }
    private void checkPrime() {
        int flag=0,n;
        for (int i = 0; i < numbers.size(); i++) {
             n = numbers.get(i);
             if (n == 0 || n == 1) {
                //Log.d(TAG, "" + n + " is not prime");
            }
            else
            {
                //if number is divisible by 2 break2<2/2=2<1 not enter into loop so if is executed and print 2
                for(int j=2;j<n/2;j++)
                    {
                    //Log.d(TAG, "" + n + "n is not prime");
                    //Log.d(TAG, "" + j + "j is not prime");
                    if(n%j==0){
                       // Log.d(TAG, "" + n + " is not prime");
                        flag = 1;
                        break;
                    }
                }
                if(flag==0)
                    Log.d(TAG, "" + n + " is prime number");
            }
        }

    }
}