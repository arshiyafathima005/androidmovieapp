package com.example.linearlogin.activity.keyframe;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import android.os.Build;
import android.os.Bundle;
import android.transition.ChangeBounds;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import com.example.linearlogin.R;

public class AnimateKeyFrameActivity extends AppCompatActivity {
    Boolean isShown=false;
    ConstraintLayout layout;
    ConstraintSet constraintSetShow=new ConstraintSet();
    ConstraintSet constraintSetHide=new ConstraintSet();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animate_key_frame);
        layout=findViewById(R.id.layoutAnimate);
        constraintSetHide.clone(layout);//contains constraint set of default layout
        constraintSetShow.clone(this,R.layout.animation_keyframe_alt);

    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void methodShow(View view) {
        Transition changeBounds=new ChangeBounds();
        changeBounds.setInterpolator(new OvershootInterpolator());
        TransitionManager.beginDelayedTransition(layout,changeBounds);
        if(isShown)
        {
            constraintSetHide.applyTo(layout);//activity layout because false
        }else
        {
            constraintSetShow.applyTo(layout);//animation key inside activity called because ture
        }
        isShown=!isShown;
    }
}