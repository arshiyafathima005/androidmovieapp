package com.example.linearlogin.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.linearlogin.R;
import com.example.linearlogin.databinding.ActivityUiNavBasicBinding;

public class UiNavBasicActivity extends AppCompatActivity {
    private ActivityUiNavBasicBinding binding;
    private NavController navController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_ui_nav_basic);
        setSupportActionBar(binding.toolBar);
        setUpNavigation();

    }

    private void setUpNavigation() {
        try {
            navController= Navigation.findNavController(this,R.id.uihostFragment);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onSupportNavigateUp()
    {
        return Navigation.findNavController(this,R.id.uihostFragment).navigateUp();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.carton_basic_activity,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==R.id.chainHome)
        {
            //getSupportFragmentManager().beginTransaction().
            // replace(R.id.hotelHost, new HotelRecyclerFragment()).commit();
            Navigation.findNavController(this,R.id.uihostFragment).navigate(R.id.createAccFragment2);
        } else if (item.getItemId()==R.id.chainNext) {
            // getSupportFragmentManager().beginTransaction().
            // replace(R.id.hotelHost, new HotelGridFragment()).commit();
            Navigation.findNavController(this,R.id.uihostFragment).navigate(R.id.loginFragment2);
        }
        else if (item.getItemId()==R.id.cartoonLast) {
            // getSupportFragmentManager().beginTransaction().
            // replace(R.id.hotelHost, new HotelGridFragment()).commit();
            Navigation.findNavController(this,R.id.uihostFragment).navigate(R.id.orderFragment);
        }
        else if (item.getItemId()==R.id.cartoonFinal) {
            // getSupportFragmentManager().beginTransaction().
            // replace(R.id.hotelHost, new HotelGridFragment()).commit();
            Navigation.findNavController(this,R.id.uihostFragment).navigate(R.id.viewAllFragment);
        }

        return super.onOptionsItemSelected(item);
    }
}