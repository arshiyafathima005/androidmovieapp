package com.example.linearlogin.activity.motion_layout;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.linearlogin.R;

public class MotionLayoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motion_layout);
    }
}