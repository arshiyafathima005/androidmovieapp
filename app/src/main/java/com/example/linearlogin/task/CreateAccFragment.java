package com.example.linearlogin.task;

import android.media.Image;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.databinding.FragmentCreateAccBinding;


public class CreateAccFragment extends Fragment {
    private FragmentCreateAccBinding binding;
    public CreateAccFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding= DataBindingUtil.inflate(inflater,R.layout.fragment_create_acc,container,false);
              Glide.with(this)
                .load("https://i.pinimg.com/originals/4c/4f/d7/4c4fd7a61bdb0f95d9e1f782ad80cf72.jpg")
                .into(binding.cartoon);
        binding.createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.loginFragment2);
            }
        });
        return binding.getRoot();
    }
}