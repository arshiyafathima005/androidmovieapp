package com.example.linearlogin.task;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.databinding.FragmentOrderBinding;


public class OrderFragment extends Fragment {
private FragmentOrderBinding binding;
    public OrderFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding= DataBindingUtil.inflate(inflater,R.layout.fragment_order,container,false);
        Glide.with(this)
                .load("https://img3.goodfon.com/wallpaper/nbig/8/f9/android-l-material-design-3707.jpg")
                .into(binding.color);
        binding.viewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.viewAllFragment);
            }
        });
        return binding.getRoot();
    }
}