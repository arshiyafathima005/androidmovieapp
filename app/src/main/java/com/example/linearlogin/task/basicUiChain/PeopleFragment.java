package com.example.linearlogin.task.basicUiChain;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.linearlogin.R;
import com.example.linearlogin.databinding.FragmentPeopleBinding;
import com.example.linearlogin.databinding.FragmentProfileBinding;


public class PeopleFragment extends Fragment {
 private FragmentPeopleBinding binding;
    public PeopleFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding= DataBindingUtil.inflate(inflater, R.layout.fragment_people,container,false);
      /*  Glide.with(this)
                .load("https://i2.wp.com/static.web-backgrounds.net/uploads/2012/08/Abstract_Green_Background_with_Floral_Elements.jpg")
                .into(binding.area);*/
        binding.cardUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.connectFragment);
            }
        });
        return binding.getRoot();
    }

}