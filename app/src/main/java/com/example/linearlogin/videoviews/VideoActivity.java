package com.example.linearlogin.videoviews;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.example.linearlogin.R;
import com.example.linearlogin.adapters.ViewPgersTwoAdapter;
import com.example.linearlogin.databinding.ActivityVideoBinding;

import java.util.ArrayList;
import java.util.List;

public class VideoActivity extends AppCompatActivity {
    private ActivityVideoBinding binding;
    private List<VideoModel> videoModels=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_video);
        getData();
    }
    private void getData() {
        VideoModel model=new VideoModel();
        model.video="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4";
        model.videoName="Elephant Dream";
        model.videoDesc="The first Blender Open Movie from 2006";
        VideoModel model2=new VideoModel();
        model2.video="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4";
        model2.videoName="Bigger JoyRiders";
        model2.videoDesc="Introducing Chromecast. The easiest way to enjoy online video and music on your TV—for the times that call for bigger joyrides. For $35. Learn how to use Chromecast with YouTube and more at google.com/chromecast.";
        VideoModel model3=new VideoModel();
        model3.video="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4";
        model3.videoName="For Bigger Meltdowns";
        model3.videoDesc="Introducing Chromecast. The easiest way to enjoy online video and music on your TV—for when you want to make Buster's big meltdowns even bigger. For $35. Learn how to use Chromecast with Netflix and more at google.com/chromecast.";
        videoModels.add(model);
        videoModels.add(model2);
        videoModels.add(model3);
        binding.videoViewPager.setAdapter(new VideoAdapter(videoModels));


    }
}