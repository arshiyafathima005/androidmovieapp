package com.example.linearlogin.videoviews;

import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.linearlogin.R;
import com.example.linearlogin.adapters.ViewPgersTwoAdapter;
import java.util.List;
public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder> {
    private List<VideoModel> videosItem;
    public VideoAdapter(List<VideoModel> videosItem) {
        this.videosItem = videosItem;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.custom_video,parent,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        VideoModel model=videosItem.get(position);
        holder.videoView.setVideoPath(model.video);
        holder.progressBar.setVisibility(View.VISIBLE);
        holder.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
                holder.progressBar.setVisibility(View.GONE);
                float videoRatio=mp.getVideoWidth()/(float)mp.getVideoHeight();
                float screenRatio=holder.videoView.getWidth()/holder.videoView.getHeight();
                /*float scale=videoRatio/screenRatio;
                if(scale>=1f)
                {
                    holder.videoView.setScaleX(scale);
                }
                else
                {
                    holder.videoView.setScaleY(scale);
                }*/
               /* holder.videoView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mp.stop();
                    }
                });*/
            }
        });
        holder.videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.start();
            }
        });

        holder.name.setText(model.videoName);
        holder.description.setText(model.videoDesc);
    }

    @Override
    public int getItemCount() {
        return videosItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView name,description;
        private VideoView videoView;
        private ProgressBar progressBar;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.videoName);
            description=itemView.findViewById(R.id.video);
            videoView=itemView.findViewById(R.id.videoView);
            progressBar=itemView.findViewById(R.id.progressBar);
        }
    }
}
