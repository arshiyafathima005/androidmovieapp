package com.example.linearlogin.model;

import android.widget.ImageView;
import android.widget.TextView;

public class RecyclerDynamicModel {
    private String recyclerText,recyclerImage,headText;

    public RecyclerDynamicModel(String headText) {
        this.headText = headText;
    }

    public RecyclerDynamicModel(String recyclerText, String recyclerImage) {
        this.recyclerText = recyclerText;
        this.recyclerImage = recyclerImage;
    }

    public String getHeadText() {
        return headText;
    }

    public void setHeadText(String headText) {
        this.headText = headText;
    }

    public String getRecyclerText() {
        return recyclerText;
    }

    public void setRecyclerText(String  recyclerText) {
        this.recyclerText = recyclerText;
    }

    public String getRecyclerImage() {
        return recyclerImage;
    }

    public void setRecyclerImage(String  recyclerImage) {
        this.recyclerImage = recyclerImage;
    }
}
