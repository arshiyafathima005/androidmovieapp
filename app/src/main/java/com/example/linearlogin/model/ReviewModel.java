package com.example.linearlogin.model;

public class ReviewModel {
    private String review,date,reviewImage;


    public String getReviewImage() {
        return reviewImage;
    }

    public ReviewModel(String review, String date, String reviewImage) {
        this.review = review;
        this.date = date;
        this.reviewImage = reviewImage;
    }

    public void setReviewImage(String reviewImage) {
        this.reviewImage = reviewImage;
    }

    public ReviewModel(String review, String date) {
        this.review = review;
        this.date = date;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }




}
