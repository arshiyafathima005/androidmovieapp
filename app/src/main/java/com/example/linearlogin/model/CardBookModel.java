package com.example.linearlogin.model;

public class CardBookModel {
    private String cardAuthor,cardTitle,cardImage;

    public CardBookModel(String cardAuthor, String cardTitle, String cardImage) {
        this.cardAuthor = cardAuthor;
        this.cardTitle = cardTitle;
        this.cardImage = cardImage;
    }

    public String getCardAuthor() {
        return cardAuthor;
    }

    public void setCardAuthor(String cardAuthor) {
        this.cardAuthor = cardAuthor;
    }

    public String getCardTitle() {
        return cardTitle;
    }

    public void setCardTitle(String cardTitle) {
        this.cardTitle = cardTitle;
    }

    public String getCardImage() {
        return cardImage;
    }

    public void setCardImage(String cardImage) {
        this.cardImage = cardImage;
    }
}
