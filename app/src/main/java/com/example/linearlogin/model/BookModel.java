package com.example.linearlogin.model;

public class BookModel {
    private String bookAuthor,bookTitle,bookImage;
    public BookModel(String bookAuthor, String bookTitle, String bookImage) {
        this.bookAuthor = bookAuthor;
        this.bookTitle = bookTitle;
        this.bookImage = bookImage;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getBookImage() {
        return bookImage;
    }

    public void setBookImage(String bookImage) {
        this.bookImage = bookImage;
    }
}
