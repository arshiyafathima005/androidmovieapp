package com.example.linearlogin.navigationProject;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.linearlogin.R;
import com.example.linearlogin.databinding.FragmentDocumentBinding;
import com.example.linearlogin.movies.activity.SharedPrefferenceConfig;


public class DocumentFragment extends Fragment {
private FragmentDocumentBinding binding;
private SharedPrefferenceConfig sp;
    public DocumentFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding= DataBindingUtil.inflate(inflater,R.layout.fragment_document,container,false);
       sp=new SharedPrefferenceConfig(getActivity());
        binding.nextDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String addharNo=binding.addharNo.getText().toString();
                String panNo=binding.panNo.getText().toString();
                sp.storeAddhar(addharNo);
                sp.storePan(panNo);
                Navigation.findNavController(v).navigate(R.id.educational);
            }
        });
        return binding.getRoot();
    }
}