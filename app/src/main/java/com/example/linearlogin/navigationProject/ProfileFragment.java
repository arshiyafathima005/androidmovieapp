package com.example.linearlogin.navigationProject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.linearlogin.R;
import com.example.linearlogin.databinding.FragmentProfileBinding;
import com.example.linearlogin.movies.activity.Moviez;
import com.example.linearlogin.movies.activity.SharedPrefferenceConfig;


public class ProfileFragment extends Fragment {
    private FragmentProfileBinding binding;
    private SharedPrefferenceConfig sp;
    public ProfileFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding= DataBindingUtil.inflate(inflater,R.layout.fragment_profile,container,false);
        sp = new SharedPrefferenceConfig(getActivity());
        binding.next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName=binding.editName.getText().toString();//get the text from user
                String emailId=binding.editEmail.getText().toString();
                sp.storeUserName(userName);//store in sp
                sp.storeEmail(emailId);
                Navigation.findNavController(v).navigate(R.id.documentFragment);
            }
        });
        return binding.getRoot();
    }
    }

