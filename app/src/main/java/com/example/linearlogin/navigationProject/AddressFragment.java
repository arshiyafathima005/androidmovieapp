package com.example.linearlogin.navigationProject;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.linearlogin.R;
import com.example.linearlogin.databinding.FragmentAddressBinding;
import com.example.linearlogin.movies.activity.SharedPrefferenceConfig;

public class AddressFragment extends Fragment {
private FragmentAddressBinding binding;
private SharedPrefferenceConfig sp;
    public AddressFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding= DataBindingUtil.inflate(inflater,R.layout.fragment_address,container,false);
       sp=new SharedPrefferenceConfig(getActivity());
        binding.nextAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String street=binding.streetName.getText().toString();
                String area=binding.areaName.getText().toString();
                String dist=binding.distName.getText().toString();
                String state=binding.stateName.getText().toString();
                String country=binding.countryName.getText().toString();
                /*sp.storeUserName(street);
                sp.storeUserName(area);
                sp.storeUserName(dist);
                sp.storeUserName(state);
                sp.storeUserName(country);*/
                Navigation.findNavController(v).navigate(R.id.reviewFragment);
            }
        });
        return binding.getRoot();
    }
}