package com.example.linearlogin.navigationProject;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.linearlogin.R;
import com.example.linearlogin.databinding.FragmentEducationalBinding;
import com.example.linearlogin.movies.activity.SharedPrefferenceConfig;


public class EducationalFragment extends Fragment {
    private FragmentEducationalBinding binding;
    //private SharedPrefferenceConfig sp;
    public EducationalFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding= DataBindingUtil.inflate(inflater,R.layout.fragment_educational,container,false);
       // sp=new SharedPrefferenceConfig(getActivity());
        binding.nextEduc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* String tenthGrade=binding.tenthGrade.getText().toString();
                String degreeGrade=binding.degreeGrade.getText().toString();
                sp.storeUserName(tenthGrade);
                sp.storeUserName(degreeGrade);*/
                Navigation.findNavController(v).navigate(R.id.addressFragment);
            }
        });
        return binding.getRoot();
    }
}