package com.example.linearlogin.navigationProject;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.linearlogin.R;
import com.example.linearlogin.databinding.FragmentReviewBinding;
import com.example.linearlogin.movies.activity.SharedPrefferenceConfig;


public class ReviewFragment extends Fragment {
private FragmentReviewBinding binding;
private SharedPrefferenceConfig sp;
    public ReviewFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding= DataBindingUtil.inflate(inflater,R.layout.fragment_review,container,false);
        sp = new SharedPrefferenceConfig(getActivity());
        String userName=sp.getUserName();
        //Toast.makeText(getActivity(),"arshiya"+sp.getUserName(),Toast.LENGTH_SHORT).show();
        String emailId=sp.getEmail();
         String adhar=sp.getAddhar();
        String pan=sp.getPan();
       /* String street=sp.getUserName();
        String area=sp.getUserName();
        String dist=sp.getUserName();
        String state=sp.getUserName();
        String country=sp.getUserName();*/
        binding.uName.setText(userName);
        binding.uEmail.setText(emailId);
     binding.adharCard.setText(adhar);
        binding.panCard.setText(pan);
     /*   binding.tenthCard.setText(userName);
        binding.degreeCard.setText(userName);
        binding.streetadd.setText(street);
        binding.areaCard.setText(area);
        binding.distCard.setText(dist);
        binding.stateCard.setText(state);
        binding.countryCard.setText(country);*/
        binding.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Submit Successful",Toast.LENGTH_LONG).show();
            }
        });
        return binding.getRoot();
    }
}